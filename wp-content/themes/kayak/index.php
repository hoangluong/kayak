<?php
get_header();
?>
<?php
$arrTaxonomies = array(2, 3);
foreach ($arrTaxonomies as $item) {
    $thisTerm = get_term($item);
    ?>
    <div class="home-product-block clearfix">
        <h3 class="block-title"><a
                href="<?php echo get_term_link($thisTerm->term_id) ?>"><?php echo $thisTerm->name ?></a> <i
                class="glyphicon glyphicon-star-empty"></i></h3>
        <div class="product-list">
            <?php
            $argProduct = array(
                'post_type' => 'san_pham',
                'numberposts' => 10,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'danh_muc',
                        'field' => 'slug',
                        'terms' => $thisTerm->slug
                    )
                )
            );
            wp_reset_query();
            $products = new WP_Query($argProduct);
            if ($products->have_posts()) {
                while ($products->have_posts()) : $products->the_post();
                    ?>
                    <div class="col-sm-3 product-item">
                        <div class="thumb"><img src="<?php the_field('image') ?>"
                                                alt="">

                        </div>
                        <div class="name"><a href=""><?php the_title() ?></a></div>
                        <div
                            class="price"><?php echo (get_field('price')) ? number_format(get_field('price')) . ' VNĐ' : ' Liên hệ' ?></div>
                        <div class="info">
                            <div class="small-image"><img src="<?php the_field('image') ?>"
                                                          alt=""></div>
                            <div class="title"><?php the_title() ?></div>
                            <ul>
                                <?php
                                if (get_field('chieu_dai')) {
                                    ?>
                                    <li><label>Chiều dài</label>: <?php the_field('chieu_dai') ?></li>
                                <?php } ?>
                                <?php
                                if (get_field('chieu_rong')) {
                                    ?>
                                    <li><label>Chiều rộng</label>: <?php the_field('chieu_rong') ?></li>
                                <?php } ?>
                                <?php
                                if (get_field('chieu_cao')) {
                                    ?>
                                    <li><label>Chiều cao</label>: <?php the_field('chieu_cao') ?></li>
                                <?php } ?>
                                <?php
                                if (get_field('can_nang')) {
                                    ?>
                                    <li><label>Cân nặng</label>: <?php the_field('can_nang') ?></li>
                                <?php } ?>
                                <?php
                                if (get_field('chat_lieu')) {
                                    ?>
                                    <li><label>Chất liệu</label>: <?php the_field('chat_lieu') ?></li>
                                <?php } ?>
                            </ul>
                            <div class="desc"><?php the_field('desc') ?></div>
                            <div class="link"><a href="<?php echo get_post_permalink(get_the_ID()) ?>">Xem chi tiết</a>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;
            }
            wp_reset_query();
            ?>

        </div>
    </div>
<?php } ?>

    <div class="home-news">
        <h3 class="block-title">Events - Sự kiện <i class="glyphicon glyphicon-star-empty"></i></h3>
        <div class="row" style="background: #dfe8e5;    padding-top: 20px;">
            <?php
            $argNews = array(
                'post_type' => 'event',
                'posts_per_page' => 3
            );
            $queryNews = new WP_Query($argNews);
            $i = 0;
            if ($queryNews->have_posts()) {
                while ($queryNews->have_posts()) : $queryNews->the_post();
                    $i++;
                    if ($i == 1) {
                        ?>
                        <div class="col-sm-8">
                            <div class="home-news">
                                <img src="<?php the_field('image') ?>" alt="">
                                <div class="info">
                                    <h3><a href="<?php echo get_permalink() ?>"><?php the_title() ?></a></h3>
                                    <div class="desc"><?php the_field('desc') ?></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                endwhile;
            }
            ?>
            <div class="col-sm-4 register">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="">
                            <img src="<?php echo get_template_directory_uri() ?>/images/dangky.png" alt="">
                        </a></div>
                    <div class="col-sm-12">
                        <a href="">
                            <img src="<?php echo get_template_directory_uri() ?>/images/dangky.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <?php
            wp_reset_query();
            ?>
        </div>
    </div>
    <div class="home-video clearfix">
        <h3><strong>Video</strong> nổi bật <i class="glyphicon glyphicon-star-empty"></i></h3>
        <div class="row">
            <?php
            $argVideos = array(
                'post_type' => 'video',
                'posts_per_page' => 3
            );
            $videos = new WP_Query($argVideos);
            $i = 0;
            if ($videos->have_posts()) {
                while ($videos->have_posts()) : $videos->the_post();
                    $i++;
                    if ($i == 1) {
                        ?>
                        <div class="col-sm-8">
                            <iframe src="https://www.youtube.com/embed/<?php the_field('video_id') ?>" frameborder="0"
                                    allowfullscreen=""></iframe>
                        </div>
                        <div class="col-sm-4">
                        <div class="row">
                        <div class="slEvents">
                        <?php
                    } else {
                        ?>
                        <div class="col-sm-12 event-second">
                            <a href="<?php the_permalink() ?>">
                                <img src="https://img.youtube.com/vi/<?php the_field('video_id') ?>/hqdefault.jpg"
                                     alt=""></a>

                        </div>
                    <?php }
                endwhile; ?>
                </div>
                </div>
                </div>
                <?php
            }
            wp_reset_query();
            ?>


        </div>
    </div>
<?php
get_footer();
?>