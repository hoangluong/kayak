<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/16/2017
 * Time: 3:39 PM
 */
get_header();

$obj = get_queried_object();
$parent = ($obj->parent) ? $obj->parent : $obj->term_id;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="row uu-dai">
            <br>
        </div>
    </div>
</div>
<div class="home-product-block clearfix">

    <h3 class="block-title"><a
            href="<?php echo get_term_link($obj->term_id) ?>"><?php echo $obj->name; ?></a> <i
            class="glyphicon glyphicon-star-empty"></i></h3>

    <div class="col-sm-3">
        <ul class="leftMenu">
            <?php
            $subcat = get_terms('danh_muc', 'hide_empty=0&parent=' . $parent);
            foreach ($subcat as $item) {
                ?>
                <li><a href="<?php echo get_term_link($item->term_id) ?>"><?php echo $item->name ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="col-sm-9">
        <div class="product-list-sub">
            <?php
            $argProduct = array(
                'post_type' => 'san_pham',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'danh_muc',
                        'field' => 'slug',
                        'terms' => $obj->slug
                    )
                )
            );

            $products = new WP_Query($argProduct);
            if ($products->have_posts()) {
                while ($products->have_posts()) : $products->the_post();
                    ?>
                    <div class="col-sm-4 product-item">
                        <div class="thumb"><img src="<?php the_field('image') ?>"
                                                alt="">
                        </div>
                        <div class="name"><a href=""><?php the_title() ?></a></div>
                        <div
                            class="price"><?php echo (get_field('price') > 0) ? number_format(get_field('price')) . ' VNĐ' : 'Liên hệ' ?> </div>
                        <div class="info">
                            <div class="small-image"><img src="<?php the_field('image') ?>"
                                                          alt=""></div>
                            <div class="title"><?php the_title() ?></div>
                            <ul>
                                <?php
                                if (get_field('chieu_dai')) {
                                    ?>
                                    <li><label>Chiều dài</label>: <?php the_field('chieu_dai') ?></li>
                                <?php } ?>
                                <?php
                                if (get_field('chieu_rong')) {
                                    ?>
                                    <li><label>Chiều rộng</label>: <?php the_field('chieu_rong') ?></li>
                                <?php } ?>
                                <?php
                                if (get_field('chieu_cao')) {
                                    ?>
                                    <li><label>Chiều cao</label>: <?php the_field('chieu_cao') ?></li>
                                <?php } ?>
                                <?php
                                if (get_field('can_nang')) {
                                    ?>
                                    <li><label>Cân nặng</label>: <?php the_field('can_nang') ?></li>
                                <?php } ?>
                                <?php
                                if (get_field('chat_lieu')) {
                                    ?>
                                    <li><label>Chất liệu</label>: <?php the_field('chat_lieu') ?></li>
                                <?php } ?>
                            </ul>
                            <div class="desc"><?php the_field('desc') ?></div>
                            <div class="link"><a href="<?php echo get_post_permalink(get_the_ID()) ?>">Xem chi tiết</a>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile;
            } else {
                echo '<div class="text-center">Chưa có sản phẩm nào!</div>';
            }
            wp_reset_query();
            ?>

        </div>
    </div>
</div>
<?php
get_footer();
?>
