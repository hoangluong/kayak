<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_kayak');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm|Jiii1puN:tY%kSfa%/4{TA-5f8[uyL%@7;W[R.Hb+r5T:9*||]cB4_2<bi:z<8');
define('SECURE_AUTH_KEY',  'gR4LT6lUp4:Pv~;#~J.{8V*OECn2#;>[=gxYkzn,d9)#/^_Mw!Er/FX1rPtG>lH)');
define('LOGGED_IN_KEY',    'u$gdF2pJSp[x>{@7>}{w~}6[D}oI|(b]{|Bq$FUYtP:79e:4z/^-H]7TRlJPYIYC');
define('NONCE_KEY',        'I&oeoB,;D7LW*2$U|yga3`=d&~[eLERz9]>Q7sqLa0`$,X>SJ-azl*BD8D1[Tqlz');
define('AUTH_SALT',        'F_#aS-+T+]`K=]U_:G`>zCSJK6%5~|z&!n;s^=#G)|${IqN_sDjx]%@[Qm8Z m^ ');
define('SECURE_AUTH_SALT', 'Vw:1_!^K/F6w Z1^(n>8e2=ar,YjI7QS*p]qWW5;lF73>X[5hJGEz-[@:}/tm+mO');
define('LOGGED_IN_SALT',   'KS:}<}Fh7}MdCSPZYFf}xijKT>4[=Jvc#EC{)Gr5s1mq,g#?g/~rww~?2J[FhRN~');
define('NONCE_SALT',       'Z.*UYh{dfS(S4B}uYML{0 xqb]]=8kZ_$Ac&+Jx|R3{`Vkj$vop/MN#m0=>02U;F');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ka_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
