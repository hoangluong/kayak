<?php
get_header();
?>
<style>
    .content-slider li {
        background-color: #ed3020;
        text-align: center;
        color: #FFF;
    }

    .content-slider h3 {
        margin: 0;
        padding: 70px 0;
    }
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="row uu-dai">
            <img src="<?php echo get_template_directory_uri() ?>/images/uu-dai.jpg" alt="">
        </div>
    </div>

    <div class="col-sm-9">
        <?php
        if (have_posts()): while (have_posts()): the_post(); ?>
            <div class="video-detail">
                <h1><?php the_title();?></h1>
                <div class="thumb"><img src="<?php the_field('image')?>" alt=""></div>
                <div class="content"> <iframe src="https://www.youtube.com/embed/<?php the_field('video_id') ?>" frameborder="0"
                                              allowfullscreen=""></iframe>
                <?php the_content()?>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>

            </div>

        <?php endwhile; endif; ?>
    </div>
    <div class="col-sm-3">
        <div class="other-product">
            <ul>
                <?php

                $arg_products_bc = array(
                    'numberposts' => 5,
                    'post_status' => 'publish',
                    'post_type' => 'video',

                );
                $products_bc = new WP_Query($arg_products_bc);
                if ($products_bc->have_posts()) {
                    while ($products_bc->have_posts()) : $products_bc->the_post();
                        ?>
                        <li class="clearfix">
                            <div>
                                <div class="col-sm-12"><a href="<?php the_permalink() ?>">
                                        <img src="https://img.youtube.com/vi/<?php the_field('video_id') ?>/hqdefault.jpg"
                                             alt=""></a></div>
                                <div class="col-sm-12 product-title"><a
                                        href="<?php the_permalink(get_the_ID()) ?>"><?php the_title() ?></a>
                                </div>
                            </div>
                        </li>
                        <?php
                    endwhile;
                }
                wp_reset_query();
                ?>
            </ul>
        </div>
    </div>
</div>

<?php get_footer(); ?>
