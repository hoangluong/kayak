<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/16/2017
 * Time: 3:39 PM
 */
get_header();

$obj = get_queried_object();

?>
<div class="home-product-block clearfix">
    <h3 class="block-title"><a
            href="#">Tin tức - Sự kiện</a> <i
            class="glyphicon glyphicon-star-empty"></i></h3>
    <div class="row">
        <div class="col-sm-9">
            <div class="events-list">
                <?php
                $argEvent = array(
                    "post_type" => 'event',
                    "status" => "publish"
                );
                $events = new WP_Query($argEvent);
                if ($events->have_posts()) {
                    while ($events->have_posts()) : $events->the_post();

                        ?>
                        <div class="event-item clearfix">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="thumb">
                                        <img src="<?php the_field('image') ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="title"><a href=""><?php the_title() ?></a></div>
                                    <div class="sapo"><?php the_field('desc') ?></div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile;
                } ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="other-product">
                <ul>
                    <?php

                    $arg_products_bc = array(
                        'numberposts' => 5,
                        'post_status' => 'publish',
                        'post_type' => 'san_pham',

                    );
                    $products_bc = new WP_Query($arg_products_bc);
                    if ($products_bc->have_posts()) {
                        while ($products_bc->have_posts()) : $products_bc->the_post();
                            ?>
                            <li class="clearfix">
                                <div>
                                    <div class="col-sm-12"><a
                                            href="<?php the_permalink(get_the_ID()) ?>"><img
                                                src="<?php the_field('image') ?>" alt=""/></a></div>
                                    <div class="col-sm-12 product-title"><a
                                            href="<?php the_permalink(get_the_ID()) ?>"><?php the_title() ?></a>
                                    </div>
                                </div>
                            </li>
                            <?php
                        endwhile;
                    }
                    wp_reset_query();
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
