</div>
<div class="bottom clearfix">
    <div class="container">
       <div class="col-sm-4">
           <ul>
               <li><a href=""><img src="<?php echo get_template_directory_uri()?>/images/logo-2.png" style="width: 100%" alt=""/></a></li>
           </ul>
       </div>
       <div class="col-sm-8 clearfix">

            <?php wp_nav_menu('footer')?>
       </div>

    </div>
</div>
</div>
<script src="<?php echo get_template_directory_uri()?>/js/jquery-1.12.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/slick.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.mousewheel.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/lightslider.min.js"></script>
<script>
    $(document).ready(function() {
        $('#image-gallery').lightSlider({
            gallery:true,
            item:1,
            thumbItem:5,
            slideMargin: 0,
            speed:500,
            auto:false,
            loop:true,
            onSliderLoad: function() {
                $('#image-gallery').removeClass('cS-hidden');
            }
        });
    });
</script>
<script>
    $('.slide-header-background, .slEvents').slick({
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 1
    });
    $('.product-list').slick({
        arrows: false,
        dots: true,
        slidesToShow: 4,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    function menuSticky() {
        var a = $(window).scrollTop();
        if (a < 150) {
            $('.header-top').removeClass('sticky');
        } else {
            $('.header-top').addClass('sticky');
        }
    }
    menuSticky();
    $(document).mousewheel(function () {
        menuSticky();
    }).scroll(function(){
        menuSticky();
    })
</script>
<?php
wp_footer();
?>
</body>
</html>