<?php
get_header();
?>
<style>
    .content-slider li {
        background-color: #ed3020;
        text-align: center;
        color: #FFF;
    }

    .content-slider h3 {
        margin: 0;
        padding: 70px 0;
    }
</style>

    <div class="row">
        <div class="col-sm-12">
            <div class="row uu-dai">
                <img src="<?php echo get_template_directory_uri() ?>/images/uu-dai.jpg" alt="">
            </div>
        </div>

        <div class="col-sm-9">
            <?php
            if (have_posts()): while (have_posts()): the_post(); ?>
            <div class="event-detail">
                <h1><?php the_title();?></h1>
                <div class="thumb"><img src="<?php the_field('image')?>" alt=""></div>
                <div class="desc"><?php the_field('desc')?></div>
                <div class="content"><?php the_content()?></div>
            </div>

            <?php endwhile; endif; ?>
        </div>
        <div class="col-sm-3">

        </div>
    </div>

<?php get_footer(); ?>
