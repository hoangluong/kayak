<?php
/*
Plugin Name: remove Parent menu
Description: Bo link o menu chinh cua website(trong truong hop co menu con)
Author: Luonghx
Version: 1.0
*/

  add_action('wp_footer', 'disable_parent_menu_link');

  function disable_parent_menu_link () {
      wp_print_scripts('jquery');
?>
      <script type="text/javascript">
          var a = $('.sub-menu').parent().children('a');
              a.css('cursor','default');
              a.removeAttr('href');

      </script> 
<?php    
  }
?>
