<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/css/slick.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/css/custom.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/css/lightslider.min.css"/>

    <?php
    wp_head();

    ?>
</head>
<body>
<div class="wrapper">
    <div class="container bg-white">
        <div class="row">
            <div class="header">
                <div class="slide-header-background">
                    <?php

                    $arg_slide = array(
                        'numberposts' => 5,
                        'post_status' => 'publish',
                        'post_type' => 'menu_slider',

                    );
                    $menuslide = new WP_Query($arg_slide);
                    if ($menuslide->have_posts()) {
                        while ($menuslide->have_posts()) : $menuslide->the_post();
                            ?>
                            <div><img src="<?php the_field('image')?>"
                                      alt=""></div>
                            <?php
                        endwhile;
                    }
                ?>

                </div>
                <div class="header-top">
                    <div class="container">
                        <div class="col-sm-3">
                            <div class="logo"><a href="<?php echo get_site_url()?>"><img src="<?php echo get_template_directory_uri()?>/images/logo.png" alt=""></a></div>
                        </div>
                        <div class="col-sm-9">
                            <div class="menu">
                                <?php wp_nav_menu(array(
                                    'menu' => 'main-menu',
                                    'menu_class' => 'main-menu',
                                    'menu_id' => 'main-menu',
                                    'container' => 'ul',
                                    'depth' => 2));
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>