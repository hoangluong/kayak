<?php
get_header();
?>
<style>
    .content-slider li {
        background-color: #ed3020;
        text-align: center;
        color: #FFF;
    }

    .content-slider h3 {
        margin: 0;
        padding: 70px 0;
    }
</style>
<div class="productDetail">
    <div class="row">
        <div class="col-sm-12">
            <div class="row uu-dai">
                <br>
            </div>
        </div>
        <div class="product-info">
            <?php
            if (have_posts()): while (have_posts()):
            the_post(); ?>
            <div class="col-sm-12">
                <div class="col-sm-7">
                    <!--    <img src="<?php echo get_field('image') ?>" alt="" style="width: 100%"/>-->
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                        <li data-thumb="<?php echo get_field('image') ?>">
                            <img src="<?php echo get_field('image') ?>"/>
                        </li>
                        <?php
                        $productGallery = get_field('gallery');
                        if (($productGallery)) {
                            foreach ($productGallery as $item) {
                                ?>
                                <li data-thumb="<?php echo $item['image'] ?>">
                                    <img src="<?php echo $item['image'] ?>"/>
                                </li>
                            <?php }
                        } ?>


                    </ul>
                </div>
                <div class="col-sm-5">
                    <h1><?php the_title() ?></h1>
                    <hr/>
                    <ul class="listAttr">
                        <li><label for=""> Tình trạng</label>: Còn hàng</li>
                        <?php if (get_field('noi_san_xuat')) { ?>
                            <li><label for=""> Sản xuất tại</label>: <?php the_field('noi_san_xuat') ?></li>
                        <?php } ?>
                        <?php if (get_field('chieu_dai')) { ?>
                            <li><label for=""> Chiều dài</label>: <?php the_field('chieu_dai') ?>
                            </li>
                        <?php } ?>

                        <?php if (get_field('chieu_rong')) { ?>
                            <li><label for=""> Chiều rộng</label>: <?php the_field('chieu_rong') ?>
                            </li>
                        <?php } ?>
                        <?php if (get_field('chieu_cao')) { ?>
                            <li><label for=""> Chiều cao</label>: <?php the_field('chieu_cao') ?>
                            </li>
                        <?php } ?>
                        <?php if (get_field('can_nang')) { ?>
                            <li><label for=""> Cân nặng</label>: <?php the_field('can_nang') ?>
                            </li>
                        <?php } ?>
                        <?php if (get_field('chat_lieu')) { ?>
                            <li><label for="">Chất liệu</label>: <?php the_field('chat_lieu') ?>
                            </li>
                        <?php } ?>
                        <?php if (get_field('xuat_su')) { ?>
                            <li><label for="">Xuất sứ</label>: <?php the_field('xuat_su') ?>
                            </li>
                        <?php } ?>
                    </ul>

                    <div class="buy">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="price">
                                    Giá: <?php echo (get_field('price')) ? number_format(get_field('price')) . ' VNĐ' : 'Liên hệ'; ?></div>
                            </div>
                            <div class="col-sm-6 text-left"><a data-fancybox data-src="#dathang" href="javascript:;"><img
                                        src="<?php echo get_template_directory_uri() . '/images/dat-hang.jpg' ?>"
                                        alt=""/></a></div>
                            <div class="col-sm-6 text-left"><a data-fancybox href="tel:0983866897"><img
                                        src="<?php echo get_template_directory_uri() . '/images/call-btn.jpg' ?>"
                                        alt=""/></a></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="col-sm-9">
            <div class="product-info">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#detail">Mô tả sản phẩm</a></li>
                        <li><a data-toggle="tab" href="#huongdan">Hướng dẫn sử dụng</a></li>
                    </ul>

                    <div class="tab-content" style="min-height: 500px">
                        <div id="detail" class="tab-pane fade in active">
                            <br>
                            <?php the_content(); ?>
                            <div class="product-gallery">
                                <?php
                                $productGallery = get_field('gallery');
                                if (($productGallery)) {
                                    foreach ($productGallery as $item) {
                                        ?>
                                        <div class="img-item">
                                            <img src="<?php echo $item['image'] ?>" alt=""/>
                                            <br/>
                                            <div><?php if ($item['description']) { ?>(<?php echo $item['description'] ?>)<?php } ?></div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div id="huongdan" class="tab-pane fade">
                            <br>
                            <?php
                            the_field('huong_dan_su_dung');
                            ?>
                        </div>

                    </div>
                    <div class="home-product-block clearfix">
                        <h3 class="block-title"><a
                                href="">Sản phẩm liên quan</a> <i
                                class="glyphicon glyphicon-star-empty"></i></h3>
                        <div class="product-list-sub">
                            <?php
                            $argProduct = array(
                                'post_type' => 'san_pham',
                                'numberposts' => 10

                            );
                            wp_reset_query();
                            $products = new WP_Query($argProduct);
                            if ($products->have_posts()) {
                                while ($products->have_posts()) : $products->the_post();
                                    ?>
                                    <div class="col-sm-4 product-item">
                                        <div class="thumb"><img src="<?php the_field('image') ?>"
                                                                alt="">

                                        </div>
                                        <div class="name"><a href=""><?php the_title() ?></a></div>
                                        <div
                                            class="price"><?php echo (get_field('price')) ? number_format(get_field('price')) . ' VNĐ' : ' Liên hệ' ?></div>
                                        <div class="info">
                                            <div class="small-image"><img src="<?php the_field('image') ?>"
                                                                          alt=""></div>
                                            <div class="title"><?php the_title() ?></div>
                                            <ul>
                                                <?php
                                                if (get_field('chieu_dai')) {
                                                    ?>
                                                    <li><label>Chiều dài</label>: <?php the_field('chieu_dai') ?></li>
                                                <?php } ?>
                                                <?php
                                                if (get_field('chieu_rong')) {
                                                    ?>
                                                    <li><label>Chiều rộng</label>: <?php the_field('chieu_rong') ?></li>
                                                <?php } ?>
                                                <?php
                                                if (get_field('chieu_cao')) {
                                                    ?>
                                                    <li><label>Chiều cao</label>: <?php the_field('chieu_cao') ?></li>
                                                <?php } ?>
                                                <?php
                                                if (get_field('can_nang')) {
                                                    ?>
                                                    <li><label>Cân nặng</label>: <?php the_field('can_nang') ?></li>
                                                <?php } ?>
                                                <?php
                                                if (get_field('chat_lieu')) {
                                                    ?>
                                                    <li><label>Chất liệu</label>: <?php the_field('chat_lieu') ?></li>
                                                <?php } ?>
                                            </ul>
                                            <div class="desc"><?php the_field('desc') ?></div>
                                            <div class="link"><a href="<?php echo get_post_permalink(get_the_ID()) ?>">Xem chi tiết</a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endwhile;
                            }
                            wp_reset_query();
                            ?>

                        </div>
                    </div>
                </div>

            </div>

        </div>
        <?php endwhile;
        endif; ?>
        <div class="col-sm-3">
            <div class="other-product">
                <ul>
                    <?php

                    $phukien = get_field('phu_kien');
                    if ($phukien) {
                        foreach ($phukien as $item) {
                           $post = get_post($item->ID);

                            ?>
                            <li class="clearfix">
                                <div>
                                    <div class="col-sm-12"><a
                                            href="<?php the_permalink() ?>"><img
                                                src="<?php the_field('image') ?>" alt=""/></a></div>
                                    <div class="col-sm-12 product-title"><a
                                            href="<?php the_permalink($item->ID) ?>"><?php the_title() ?></a>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                    }
                    wp_reset_query();
                    ?>
                </ul>

            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
