<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/16/2017
 * Time: 3:39 PM
 */
get_header();

$obj = get_queried_object();

?>
<div class="home-product-block clearfix">
    <h3 class="block-title"><a
            href="#">Tin tức - Sự kiện</a> <i
            class="glyphicon glyphicon-star-empty"></i></h3>
    <div class="row">
        <div class="col-sm-9">
            <div class="events-list">
                <?php
                $argVideo = array(
                    "post_type" => 'video',
                    "status" => "publish"
                );
                $videos = new WP_Query($argVideo);
                if ($videos->have_posts()) {
                    while ($videos->have_posts()) : $videos->the_post();

                        ?>
                        <div class="event-item clearfix">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="thumb">
                                        <img src="<?php the_field('image') ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="title"><a href=""><?php the_title() ?></a></div>
                                    <div class="sapo"><?php the_field('desc') ?></div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile;
                } ?>
            </div>
        </div>
        <div class="col-sm-3">

        </div>
    </div>
</div>
<?php
get_footer();
?>
