<?php
/**
 * Kayak functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Kayak
 * @since Kayak 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Kayak 1.0
 */
if (!isset($content_width)) {
    $content_width = 660;
}

/**
 * Kayak 1.0 only works in WordPress 4.1 or later.
 */
if (version_compare($GLOBALS['wp_version'], '4.1-alpha', '<')) {
    require get_template_directory() . '/inc/back-compat.php';
}

if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}
register_nav_menus(array(
    'primary' => __('Primary Menu'),
    'social' => __('Social Links Menu')
));

